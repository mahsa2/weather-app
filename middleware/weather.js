const http = require('http')
const mongoose = require('mongoose')

const constants = require('../config/constants')
const generatorWrap = require('../lib/generatorWrap')

const Weather = mongoose.model('weather')

/*
 * Sends request to the weather API with the requested options
 *
 * @arg apiOptions  Object  The json object contains the following
 *                  Reference of formats: https://www.wunderground.com/weather/api/d/docs?d=data/index&MR=1
 *                  {
 *                      feature: The feature with the weather API request format
 *                      setting: (Optional) The setting with the weather API request format
 *                      query: The query with the weather API request format
 *                  }
 * @return Object  The promise object contains the weather report
 */
function getWeatherReport(apiOptions) {
    return new Promise(function(resolve, reject) {
        apiOptions = apiOptions || {}

        const options = {
            method: 'GET',
            port: 80,
            path: `${constants.WEATHER_API_PATH}/${apiOptions.attributes}/q/${apiOptions.query}.json`,
            hostname: constants.WEATHER_API_HOST
        }

        const req = http.request(options, (res) => {
            res.setEncoding('utf8')

            let response = ''
            res.on('data', (chunk) => {
                response += chunk
            })
            res.on('end', () => {
                resolve(JSON.parse(response))
            })
        })
        
        req.on('error', (e) => {
            reject(new Error(`problem with the request: ${e.message}`))
        })

        req.end()
    })
}

/*
 * Stores or update the weather report information in the database
 *
 * @arg weatherReport  Object  The Json object contains the weather report
 * @arg language       String  The language of the report
 */
function storeWeatherConditionReport(weatherReport, language) {
    return new Promise(function(resolve, reject) {
        const conditions = {
            language,
            'display_location.city': weatherReport.current_observation.display_location.city,
            'display_location.country': weatherReport.current_observation.display_location.country
        }

        // Update the document if it exists, and if not, insert a new one
        Weather.update(conditions, { ...weatherReport.current_observation, language }, { upsert: true }, (err) => {
            if (err) return reject(err)
            resolve()
        })
    })
}

///////////////////////////////////////////////////////

/*
 * The middleware function for calling the weather API to get the report.
 * The report will be send as a JSON response
 *
 * @arg req  Object   The current request object
 * @arg res  Object   The response object
 * @arg next Function The next handler function in Express
 */
module.exports = exports = generatorWrap(function* (req, res, next) {
    if (!req.params.attributes) return next(new Error('No attributes (features/settings) specified'))
    if (!req.query.location) return next(new Error('No `location` specified in the query'))

    const apiOptions = {
        attributes: req.params.attributes,
        query: req.query.location
    }

    const weatherReport = yield getWeatherReport(apiOptions)
    if (weatherReport.response.error || !weatherReport.current_observation) {
        return res.json(weatherReport)
    }

    // In order to show a sample of database storing, I have just recorded part of the condition report in the databse
    if (req.params.attributes.indexOf('conditions') > -1) {
        // Retrieve the language
        const matchRes = req.params.attributes.match(/.*?lang:(.*?)(\/.*|$)/)
        const language = matchRes ? matchRes[1] : 'EN' 
        
        yield storeWeatherConditionReport(weatherReport, language)
    }

    res.json(weatherReport)
})