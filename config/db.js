const mongoose = require('mongoose')

const constants = require('./constants')
const WeatherSchema = require('../schemas/weather')

require('./models')

mongoose.Promise = global.Promise

if (process.env.ENVIRONMENT === 'test') {
    require('../tests/config/db')
} else {
    mongoose.connect(`mongodb://localhost/${constants.DB_NAME}`)
    mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error'))
    mongoose.connection.once('open', console.log.bind(console, 'Successfully connected to MongoDB'))
}

mongoose.set('debug', process.env.DEBUG_MONGOOSE === 'true' || false)

