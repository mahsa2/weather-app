exports.DB_NAME = 'weatherApp'

exports.API_PORT = 3333

const WEATHER_API_KEY = ''

if (!WEATHER_API_KEY) {
	console.error('You must set WEATHER_API_KEY before running this app.')
	process.exit(0)
}

exports.WEATHER_API_HOST = 'api.wunderground.com'
exports.WEATHER_API_PATH = `/api/${WEATHER_API_KEY}`