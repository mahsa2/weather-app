const Promise = require('bluebird')

process.on('unhandledRejection', function(reason, promise) {
	console.log(`unhandledRejection: ${reason}-${promise}`)
})

process.on('rejectionHandled', function(promise) {
    console.log(`rejectionHandled: ${promise}`)
})

module.exports = exports = (genFn) => {
    const cr = Promise.coroutine(genFn)
    return (req, res, next) => {
        cr(req, res, next).catch(next)
    }
}