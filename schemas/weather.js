const mongoose = require('mongoose')
const Schema = mongoose.Schema
 
// a unique id is generated in the mongoDB
const WeatherSchema = new Schema({
    language: { type: String },
    display_location: {
        city: { type: String },
        country: { type: String }
    },
    observation_time: { type: String },
    weather: { type: String },
    temperature_string: { type: String },
    feelslike_string: { type: String }
},{
    toObject: {
        transform(doc, ret) {
          ret.id = ret._id.toString()
          delete ret._id
        }
    }
})

// Setting index on the values
WeatherSchema.index({ language: 1, 'display_location.city': 1 , 'display_location.country': 1})

module.exports = exports = WeatherSchema