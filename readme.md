## Documentation

### Install npm and Node.js
```
sudo apt-get install python-software-properties
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash 
sudo apt-get install -y nodejs
```

### Install mongoDB
I ran the program on Ubuntu. Based on your OS you need to install the mongoDB from [here](https://docs.mongodb.com/manual/administration/install-community/)

### Start the program
+ `npm Install` to install dependencies
+ `npm run app` to run the application in the command line
+ `npm run test` to run the tests
+ Then you can run `http://localhost:3333/` in a browser to see the reports on a table

#### API calls
You can try the following standards way to get the reports.
Note: The following values are the same values possible in [the wunderground API website](https://www.wunderground.com/weather/api/d/docs?d=data/index&MR=1).
```
http://localhost:3333/api/weather/report/**LIST OF FEATURES**?location=**QUERY**
http://localhost:3333/api/weather/report/**LIST OF FEATURES**/**LIST OF SETTINGS**?location=**QUERY**
```

##### Examples
```
http://localhost:3333/api/weather/report/forecast?location=Australia/Sydney
http://localhost:3333/api/weather/report/forecast/lang:FR?location=Australia/Sydney
http://localhost:3333/api/weather/report/conditions/geolookup/lang:FR/pws:0/?location=CA/San_Francisco
```