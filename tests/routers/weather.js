const chakram = require('chakram')
const mockgoose = require('mockgoose')

const constants = require('../../config/constants')

const expect = chakram.expect

describe('Weather Report Tests', function () {
    const HOST = `http://localhost:${constants.API_PORT}`
    const PATH = '/api/weather'
    const REPORT_PATH = `${PATH}/report`
    const LOCATION='CA/San_Francisco'
    const CITY = 'San Francisco'

    beforeEach(done => {
        mockgoose.reset(done)
    })

    it('should fetch the weather conditions report and store it in the database', function () {
        this.timeout(15000)

        const response = chakram.get(`${HOST}${REPORT_PATH}/conditions?location=${LOCATION}`)
        expect(response).to.have.status(200)
        expect(response).to.have.header('content-type', 'application/json; charset=utf-8')
        expect(response).to.have.json('current_observation.display_location.city', CITY)
        expect(response).to.have.json('response.features.conditions', 1)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body).to.be.an.array
                expect(getResponse.body[0]).to.have.deep.property('display_location.city', CITY)
            })
        })
    })

    it('should fetch multiple features from the weather api and do not store anything in the database', function () {
        this.timeout(15000)

        const response = chakram.get(`${HOST}${REPORT_PATH}/geolookup/forecast?location=${LOCATION}`)
        expect(response).to.have.status(200)
        expect(response).to.have.header('content-type', 'application/json; charset=utf-8')
        expect(response).to.have.json('response.features.geolookup', 1)
        expect(response).to.have.json('response.features.forecast', 1)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body).to.be.an.array
                expect(getResponse.body).to.be.empty
            })
        })
    })

    it('should fetch multiple features from the weather api including the `conditions`, and store them in the database', function () {
        this.timeout(15000)

        const response = chakram.get(`${HOST}${REPORT_PATH}/geolookup/conditions?location=${LOCATION}`)
        expect(response).to.have.status(200)
        expect(response).to.have.header('content-type', 'application/json; charset=utf-8')
        expect(response).to.have.json('current_observation.display_location.city', CITY)
        expect(response).to.have.json('response.features.geolookup', 1)
        expect(response).to.have.json('response.features.conditions', 1)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body).to.be.an.array
                expect(getResponse.body[0]).to.have.deep.property('display_location.city', CITY)
            })
        })
    })

    it('should store a conditions report with the requested language at the end', function () {
        this.timeout(15000)

        const LANGUAGE = 'FR'
        const response = chakram.get(`${HOST}${REPORT_PATH}/conditions/lang:${LANGUAGE}?location=${LOCATION}`)
        expect(response).to.have.status(200)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}/${LANGUAGE}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body[0]).to.have.deep.property('language', LANGUAGE)
            })
        })
    })

    it('should store a conditions report with the requested language included with other settings', function () {
        this.timeout(15000)

        const LANGUAGE = 'FR'
        const response = chakram.get(`${HOST}${REPORT_PATH}/conditions/lang:${LANGUAGE}/pws:1/?location=${LOCATION}`)
        expect(response).to.have.status(200)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}/${LANGUAGE}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body[0]).to.have.deep.property('language', LANGUAGE)
            })
        })
    })

    it('should store the conditions report with the default language if no language specified', function() {
        this.timeout(15000)

        const response = chakram.get(`${HOST}${REPORT_PATH}/conditions?location=${LOCATION}`)
        expect(response).to.have.status(200)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body[0]).to.have.deep.property('language', 'EN')
            })
        })
    })

    it('should work with multiple features and multiple settings together', function(){
        this.timeout(15000)

        const LANGUAGE = 'FR'
        const response = chakram.get(`${HOST}${REPORT_PATH}/geolookup/conditions/lang:${LANGUAGE}/pws:1?location=${LOCATION}`)
        expect(response).to.have.status(200)
        expect(response).to.have.json('current_observation.display_location.city', CITY)
        expect(response).to.have.json('response.features.conditions', 1)
        expect(response).to.have.json('response.features.geolookup', 1)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}/${LANGUAGE}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body[0]).to.have.deep.property('language', LANGUAGE)
            })
        })
    })

    it('should insert a new document to the database and the next one should only updates the document', function() {
        this.timeout(15000)

        const response1 = chakram.get(`${HOST}${REPORT_PATH}/conditions?location=${LOCATION}`)
        expect(response1).to.have.status(200)

        const response2 = chakram.get(`${HOST}${REPORT_PATH}/conditions?location=${LOCATION}`)
        expect(response2).to.have.status(200)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body).to.have.lengthOf(1)
            })
        })
    })

    it('should report an error if no features is set', function() {
        this.timeout(15000)

        return chakram.get(`${HOST}${REPORT_PATH}/?location=${LOCATION}`)
        .then((response) => {
            expect(response).to.have.status(500)
            expect(response.body).to.have.string('No attributes (features/settings) specified')
        })
    })

    it('should throw an error if no location is set', function() {
        this.timeout(15000)

        return chakram.get(`${HOST}${REPORT_PATH}/conditions`)
        .then((response) => {
            expect(response).to.have.status(500)
            expect(response.body).to.have.string('No `location` specified in the query')
        })
    })

    it('should handle the errors from the weather API when the features are incorrect', function() {
        this.timeout(15000)

        const response = chakram.get(`${HOST}${REPORT_PATH}/incorrect_feature?location=${LOCATION}`)
        expect(response).to.have.status(200)
        expect(response).to.have.json('response.error.type', 'unknownfeature')

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body).to.be.empty
            })
        })
    })

    it('should handle the result of having multiple reports for a state', function() {
        this.timeout(15000)

        const response = chakram.get(`${HOST}${REPORT_PATH}/conditions?location=NY`)
        expect(response).to.have.status(200)
        expect(response).to.have.json('response.features.conditions', 1)

        return chakram.wait().then(() => {
            return chakram.get(`${HOST}${PATH}`)
            .then((getResponse) => {
                expect(getResponse).to.have.status(200)
                expect(getResponse.body).to.be.empty
            })
        })
    })
}) 