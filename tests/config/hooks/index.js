require('../../../')
const mongoose = require('mongoose')

// Once the database is connectted, run the tests
mongoose.connection.once('open', run)

after((done) => {
    if (mongoose.isMocked === true) {
        mongoose.unmock(done)
    }
})