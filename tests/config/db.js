const mongoose = require('mongoose')
const mockgoose = require('mockgoose')

mockgoose(mongoose).then(() => {
    mongoose.connect('mongodb://localhost/mockedTestDd')
    mongoose.connection.on('error', console.error.bind(console, 'Test MongoDB connection error'))
    mongoose.connection.once('open', console.log.bind(console, 'Successfully connected to Test MongoDB'))
})