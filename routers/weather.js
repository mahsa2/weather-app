const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()

const generatorWrap = require('../lib/generatorWrap')
const middleware = require('../middleware')
const Weather = mongoose.model('weather')

// Endpoints on the API
router.get('/weather/report/:attributes(*)', middleware.callWeatherAPI)
router.get('/weather/:language?', generatorWrap(function* (req, res) {
    const language = (req.params.language) ? req.params.language : 'EN'
    const weathers = yield Weather.find({ language })
    res.json(weathers)
}))

module.exports = exports = router