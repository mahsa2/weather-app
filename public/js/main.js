var HOST = 'http://localhost:3333'
// When the DOM is ready, this function will be called
$(document).ready(function() {
  getReport('EN')
  $('#languages').append(makeLink('', 'EN')).append(makeLink('', 'FR'))
  $('#languages a').click(function(e) {
    // Avoid the default behaviour of the click
    e.preventDefault()

    $('#report-table').find('tr:gt(0)').remove()
    getReport($(this).text())
  })
})

function getReport(language) {
  $.ajax({
    type: 'GET',
    url: HOST + '/api/weather/' + language,
    data: '{}',
    contentType: 'application/json charset=utf-8',
    dataType: 'json',
    cache: false,
    success: function(data) {
      var tr = ''
      $.each(data, function (i, item) {
        tr += '<tr>'
          + makeTableData(item.display_location.country + '/' + item.display_location.city)
          + makeTableData(item.weather)
          + makeTableData(item.temperature_string)
          + makeTableData(item.feelslike_string)
          + makeTableData(item.observation_time)
          + '</tr>'
      })
    
      $('#report-table').append(tr)
    },
    error: function (msg) {
      alert('error', msg.responseText)
    }
  })
}

function makeTableData(val) {
  return '<td>' + val + '</td>'
}

function makeLink(src, val) {
  return '<a href="' + src + '">' + val + '</a>'
}
