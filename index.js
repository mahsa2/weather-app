global.Promise = require('bluebird')
require('./config/db')

const express = require('express')
const path = require('path')
const weather = require('./routers/weather')

const constants = require('./config/constants')

const api = express()

api.use((err, req, res, next) => {
    // console.error(err.stack)
    res.status(500).send('Internal Error')
})

// Landing page
api.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public/index.html'))
})
api.use('/api', weather)

// Mapping the public folder to static route to be able to access the public contents
api.use('/static', express.static(path.join(__dirname, 'public')))


api.listen(constants.API_PORT)